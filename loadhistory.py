import sqlite3
import json
import ccxt
from datetime import datetime
import time
import logging

# задаём уровень логгирования
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')

def load_history():
    # создаём объект с именем модуля
    log = logging.getLogger('loadhistory')

    log.info("Download and  update DB")
    con = sqlite3.connect("a2bot.db")
    cur = con.cursor()
    with open("config.json", "r") as read_file:
        conf = json.load(read_file)

    nday = conf['history_days']
    # времяв БД хранится в мс
    starttime = int(time.time()) * 1000 - nday * 24 * 60 * 60 * 1000

    log.info("Create tables")
    for e in conf['exchanges']:
        # get instance exchange
        exchange_id = e
        exchange_class = getattr(ccxt, exchange_id)
        exchange = exchange_class()
        exchange.enableRateLimit = True
        exchange.load_markets()

        limit = conf['exchanges'][e]['limit']
        for s in conf['exchanges'][e]['pairs']:
            log.info('-' * 80)
            db_name = f"{e}_{s}"
            log.info(f"Try create table '{db_name}'")
            cur.execute(f"""
                CREATE TABLE IF NOT EXISTS '{db_name}'
                (dt INTEGER NOT NULL,  o REAL, h REAL, l REAL, c REAL, v REAL,
                 PRIMARY KEY (dt)
                 )
                """)
            con.commit()
            log.info(f"Download symbol '{s}'")
            log.info(f"Delete data older than {nday} days")
            cur.execute(f"delete from '{db_name}' where dt < {starttime}")

            since = starttime
            symbol = s
            while since < exchange.milliseconds():
                # проверим, загруженны ли данные в таблицу за этот диапазон дат
                endtime = since + limit * 60 * 1000
                sql = f"select count(*) from '{db_name}' where (dt between {since} and {endtime}) "
                cur.execute(sql)
                nrate = cur.fetchone()[0]
                # если количество данных равно limit, то эти данные загруженны, пропускаем
                if nrate is None or nrate < limit:
                    try:
                        orders = exchange.fetch_ohlcv(symbol=symbol, timeframe='1m', since=since, limit=limit)
                    except Exception as e:
                        log.error(f"{e}. Exit...")
                        break

                    if len(orders) == 0:
                        log.error(f"Get NULL responce from '{e}:{s}'. Exit...")
                        break
                    t1 = datetime.fromtimestamp(orders[0][0] / 1000).strftime('%Y-%m-%d %H:%M:%S')
                    t2 = datetime.fromtimestamp(orders[len(orders) - 1][0] / 1000).strftime('%Y-%m-%d %H:%M:%S')
                    log.info(f"{e}:{s}: [{t1}] - [{t2}] save")
                    # сохранить котировки в БД
                    for i in orders:
                        cur.execute(
                            f"insert or ignore into '{db_name}' values "
                            f"('{i[0]}','{i[1]}','{i[2]}','{i[3]}','{i[4]}','{i[5]}')")
                    con.commit()

                    if len(orders):
                        since = orders[len(orders) - 1][0]
                    else:
                        break
                else:
                    t1 = datetime.fromtimestamp(since / 1000).strftime('%Y-%m-%d %H:%M:%S')
                    t2 = datetime.fromtimestamp(endtime / 1000).strftime('%Y-%m-%d %H:%M:%S')
                    log.info(f"{e}:{s}: [{t1}] - [{t2}] pass")
                    since = endtime

load_history()
