# from typing import List

import app
import time
import sqlite3
import re
from apscheduler.schedulers.background import BackgroundScheduler
# from apscheduler.schedulers.gevent import GeventScheduler


def get_last_date(exch):
    conn = sqlite3.connect("a2bot.db")
    with conn:
        cur = conn.cursor()
        rz = cur.execute(f"select max(dt) from {exch}")
        maxdate = cur.fetchone()[0]
        cur.close()
    return maxdate



def update_db(exch, symbols, deep = 24*60*60):
    exchobj = app.get_exch(exch)
    conn = sqlite3.connect("a2bot.db")
    with conn:
        cur = conn.cursor()
        limit = app.CONFIG['history_days']
        for s in symbols['pairs']:
            db_name = f"{exchobj.id}_{s}"
            sql = f"select max(dt) from '{db_name}'"
            cur.execute(sql)
            lastdate = cur.fetchone()[0]
            # TODO вариант когда не загружены котировки - загружаем сутки
            if lastdate is None or lastdate == 0:
                lastdate = (int(time.time()) - deep)*1000
            since = lastdate
            while since < exchobj.milliseconds():
                symbol = s
                try:
                    orders = exchobj.fetch_ohlcv(symbol=symbol, timeframe='1m', since=since, limit=limit)
                except Exception as e:
                    break
                # сохранить котировки в БД
                for i in orders:
                    sql = f"insert or ignore into '{db_name}' values " \
                          f"('{i[0]}', '{i[1]}','{i[2]}','{i[3]}','{i[4]}','{i[5]}')"
                    cur.execute(sql)
                conn.commit()

                if len(orders):
                    since = orders[len(orders) - 1][0]
                else:
                    break
        cur.close()


def tf2a(timeframe):
    """
    Парсим таймфрейм в массив
    :param timeframe:string таймфрейм из Pandas
    :return:array массив из 2-х элементов
    """
    tf = re.split('(\D+)', timeframe)
    return [int(tf[0]), tf[1]]


#     Start the scheduler
scheduler = BackgroundScheduler()
scheduler.start()

@scheduler.scheduled_job('cron', minute='*/1')
def get_rate():
    exch = app.CONFIG['exchanges']
    for e in exch:
        update_db(e,exch[e])
