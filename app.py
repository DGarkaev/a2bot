#!/usr/bin/python3

import os
import sys
os.chdir(sys.path[0])

import tempfile
import ccxt
import threading
import sqlite3
import json
import base64

from flask import Flask, render_template, jsonify, request, send_file
from gevent.pywsgi import WSGIServer
import pandas as pd
from flask_cors import CORS
import time
import datetime
import dateutil
import common
import pytz
import calendar





# ******************** INIT ********************
EXCH_MUTEX = threading.Lock()
BD_MUTEX = threading.Lock()

# кешированный список бирж
EXCHS = {}

with open("config.json", "r") as read_file:
    CONFIG = json.load(read_file)

CONN = sqlite3.connect("a2bot.db")  # или :memory: чтобы сохранить в RAM
cur = CONN.cursor()
for e in CONFIG['exchanges']:
    for s in CONFIG['exchanges'][e]['pairs']:
        db_name = f"{e}_{s}"
        cur.execute(f"""
            CREATE TABLE IF NOT EXISTS '{db_name}'
            (dt INTEGER NOT NULL,  o REAL, h REAL, l REAL, c REAL, v REAL,
             PRIMARY KEY (dt)
             )
            """)
    CONN.commit()
cur.close()

# ******************** INIT END ********************

app = Flask(__name__)
CORS(app)


# получить список доступных бирж
@app.route('/exchanges')
def get_exchs():
    return jsonify(ccxt.exchanges)


# получить список котируемых пар на бирже
@app.route('/pairs/<string:exch>')
def get_pairs(exch: str):
    exchange = get_exch(exch)
    pairs = exchange.symbols
    return jsonify(pairs)


# если биржа есть в списке - возвращаем её,
# если нет - добавляем в список и возвращаем
def get_exch(exch: str):
    global EXCH_MUTEX
    with EXCH_MUTEX:
        if exch in EXCHS:
            return EXCHS[exch]
        exchange_id = exch
        exchange_class = getattr(ccxt, exchange_id)
        exchange = exchange_class()
        exchange.enableRateLimit = True
        exchange.load_markets()
        EXCHS[exch] = exchange
        return EXCHS[exch]


@app.route('/data')
def data():
    # TODO получение котировок сделать через потоки
    frmt = request.args.get('format', 'json').lower()
    if frmt not in ['json', 'xlsx']:
        frmt = 'json'

    exch = request.args.get('exch').lower()

    pairs = request.args.get('pairs').split(',')

    ma = request.args.get('ma', None)
    if ma is not None:
        ma = [int(m) for m in ma.split(',')]  # .lower()

    # получаем таймфрейм
    tf = request.args.get('tf', '15T')
    atf = common.tf2a(tf)

    # limit = int(request.args.get('limit', 100))
    # if ma is not None:
    #     limit = max([limit, max(ma)])

    #если начальная дата не задана - берем за 3 дня
    start_date = request.args.get('startdate', None)
    if start_date is None:
        start_date = datetime.date.today()-datetime.timedelta(days=3)
    else:
        start_date = dateutil.parser.parse(start_date)
    # все вычисления в GMT
    # start_date=start_date.replace(tzinfo=pytz.UTC)
    start_date = calendar.timegm(start_date.timetuple())*1000


    # пересчитаем лимит в секунды
    # k = 1
    # if atf[1] == 'T':
    #     k = 60
    # if atf[1] == 'H':
    #     k = 60 * 60
    # if atf[1] == 'D':
    #     k = 24 * 60 * 60

    # sec_limit = atf[0] * k * limit

    exchange = get_exch(exch)

    # получим текущее время, для того чтобы все данные были синхронизированны по времени,
    # т.к. может произойти переход на след. минуту пока будем получать данные
    #start_time = (int(time.time()) - sec_limit) * 1000
    end_date = int(time.time()) * 1000

    with sqlite3.connect("a2bot.db") as conn:
        gdf = pd.DataFrame()
        for sp in pairs:
            db_name = f"{exchange.id}_{sp}"
            sql = f"select dt, c as r from '{db_name}' where dt>= {start_date} and dt<={end_date}"
            df = pd.read_sql(sql=sql, con=conn, index_col='dt', parse_dates={'dt': 'ms'})
            df['r'] = pd.to_numeric(df['r'], errors='coerce')
            df['r'] =df['r'].ffill().bfill()
            #посчитаем процент отклонения от первого значения
            fvalue = df['r'][0]
            df['r']= (df['r']-fvalue)/fvalue*100
            gdf[sp] = df['r']

    # певая колонка - это индекс

    # TODO columns возможно заменить на iloc
    gdf = gdf.resample(tf, label='right').mean()

    # вычислим среднее со 2 колонки и до последней
    gdf['mean'] = gdf[gdf.columns[1:]].mean(axis=1)
    #     | A     |
    # D = |--- - 1| * 100
    #     | B     |
    #gdf['deviation'] = (gdf['mean'] / gdf[gdf.columns[0]] - 1) * 100.0
    gdf['deviation'] = gdf['mean'] - gdf[gdf.columns[0]]

    # если для MA нет параметров - то не считаем
    if ma is not None:
        for i in ma:
            gdf[f"ma{i}"] = gdf['deviation'].rolling(window=int(i)).mean()

    if frmt == 'json':
        djson = gdf.to_json(orient='index', date_format='epoch')
        response = app.response_class(
            response=djson,
            status=200,
            mimetype='application/json')
        return response

    if frmt == 'xlsx':
        fp = tempfile.TemporaryFile()
        gdf.to_excel(fp, startrow=0, merge_cells=False, sheet_name=exch)
        fp.seek(0)
        return send_file(fp,
                mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                         attachment_filename=f"{exch}-{int(time.time())}.xlsx",
                         as_attachment=True)
        # b64 = base64.b64encode(fp.read())
        # response = app.response_class(
        #     response={"data": b64},
        #     status=200,
        #     mimetype='application/json')
        # return response


@app.route('/reloadconfig')
def reloadconfig():
    global CONFIG
    with open("config.json", "r") as read_file:
        CONFIG = json.load(read_file)


@app.route('/loadhistory/<string:exch>/<string:symbol>')
def loadhistory(exch: str, symbol: str):
    common.load_history(get_exch(exch), symbol)
    return jsonify({"msg": "Done"})


if __name__ == '__main__':
    http_server = WSGIServer(('', 3211), app)
    http_server.serve_forever()
